use clap::{App, AppSettings, Arg, ArgMatches, Shell, SubCommand};
use hashbrown::{HashMap, HashSet};
use rayon::prelude::*;
use termion::{color, style};
use walkdir::{DirEntry, WalkDir};
use yaml_rust::{Yaml, YamlEmitter, YamlLoader};

use std::collections::VecDeque;
use std::env::{current_dir, set_current_dir};
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, Read, Write};
use std::path::{Path, PathBuf};
use std::process::{exit, Child, Command, Stdio};
use std::time::SystemTime;

static CONFIG_NAME: &str = "asd_config.yaml";

fn build_cli() -> App<'static, 'static> {
    App::new("asd")
        .version("0.1")
        .author("Matthew Williamson <matthewwilliamon93@gmail.com>")
        .about("A simple C++ build tool")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .subcommand(
            SubCommand::with_name("build").about("Builds a exe").arg(
                Arg::with_name("release")
                    .help("Flag for building in release mode")
                    .short("r")
                    .long("release"),
            ),
        )
        .subcommand(
            SubCommand::with_name("test")
                .about("Builds a test exe")
                .arg(
                    Arg::with_name("release")
                        .help("Flag for building in release mode")
                        .short("r")
                        .long("release"),
                ),
        )
        .subcommand(SubCommand::with_name("check").about("Semantically checks the project"))
        .subcommand(
            SubCommand::with_name("clean").about("Cleans out all the temp files in the project"),
        )
        .subcommand(
            SubCommand::with_name("run")
                .about("Builds and runs the exe")
                .arg(
                    Arg::with_name("release")
                        .help("Flag for building in release mode")
                        .short("r")
                        .long("release"),
                )
                .arg(
                    Arg::with_name("arguments")
                        .help("Arguments passed to the executable")
                        .long("args")
                        .takes_value(true)
                        .multiple(true)
                        .required(false),
                ),
        )
        .subcommand(
            SubCommand::with_name("new")
                .about("Creates a new config file for the project")
                .arg(
                    Arg::with_name("name")
                        .help("Name for the projects config")
                        .index(1)
                        .required(true),
                )
                .arg(
                    Arg::with_name("pkgs")
                        .help("Packages to be included")
                        .short("p")
                        .long("pkgs")
                        .multiple(true)
                        .takes_value(true)
                        .required(false),
                )
                .arg(
                    Arg::with_name("symbols")
                        .help("Symbols to be included")
                        .short("s")
                        .long("symbols")
                        .multiple(true)
                        .takes_value(true)
                        .required(false),
                ),
        )
        .subcommand(SubCommand::with_name("fmt").about("Formats given files"))
        .subcommand(SubCommand::with_name("lint").about("Lints given files"))
        .subcommand(
            SubCommand::with_name("completions")
                .about("Generates autocompletions for the shell")
                .arg(
                    Arg::with_name("shell")
                        .help("Name of the shell")
                        .index(1)
                        .possible_values(&["bash", "fish", "zsh"])
                        .required(true),
                ),
        )
}

fn error(msg: &str) {
    eprintln!(
        "{}{}error:{}{} {}",
        color::Fg(color::Red),
        style::Bold,
        color::Fg(color::Reset),
        style::Reset,
        msg
    );
}

fn progress(msg: &str) {
    println!(
        "{}{}    {}{}{}",
        color::Fg(color::Blue),
        style::Bold,
        msg,
        color::Fg(color::Reset),
        style::Reset
    );
}

fn success(msg: &str) {
    println!(
        "{}{}{}{}{}",
        color::Fg(color::Green),
        style::Bold,
        msg,
        color::Fg(color::Reset),
        style::Reset
    );
}

fn display_errors(process: &mut Child) {
    let reader = BufReader::new(process.stderr.as_mut().unwrap());
    for line in reader.lines() {
        eprintln!("{}", line.unwrap());
    }
}

fn find_config() -> PathBuf {
    let mut dir = current_dir().unwrap();
    loop {
        if dir.to_str().unwrap() == "/" {
            break;
        }
        let mut config = dir.clone();
        config.push(&CONFIG_NAME);
        if config.exists() {
            let _ = set_current_dir(&dir);
            return config;
        }
        let _ = dir.pop();
    }
    error("No config file could be found.\nPlease create one with asd new.");
    exit(1);
}

struct Config {
    target: String,
    pkgs: Vec<String>,
    symbols: Vec<String>,
}

impl Config {
    fn new(target: String, pkgs: Vec<String>, symbols: Vec<String>) -> Config {
        Config {
            target,
            pkgs,
            symbols,
        }
    }
}

fn yaml_to_config(s: &str) -> Config {
    let yamls = YamlLoader::load_from_str(s).unwrap();
    let yaml = &yamls[0];
    let target = yaml["target"].as_str().unwrap().to_string();
    let yaml_to_vec = |y: &Yaml| {
        y.as_vec()
            .unwrap()
            .iter()
            .map(|i| i.as_str().unwrap().to_string())
            .collect()
    };
    let pkgs = yaml_to_vec(&yaml["pkgs"]);
    let symbols = yaml_to_vec(&yaml["symbols"]);
    Config::new(target, pkgs, symbols)
}

fn get_config() -> io::Result<Config> {
    let config = find_config();
    let mut file = File::open(&config)?;
    let mut s = String::new();
    let _ = file.read_to_string(&mut s);
    Ok(yaml_to_config(&s))
}

fn non_hidden_dirs(entry: &DirEntry) -> bool {
    entry.metadata().unwrap().is_dir()
        && entry
            .file_name()
            .to_str()
            .map(|s| entry.depth() == 0 || !s.starts_with('.'))
            .unwrap_or(false)
}

fn get_all_dirs() -> Vec<String> {
    WalkDir::new(".")
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| non_hidden_dirs(e))
        .map(|dir| dir.path().to_str().unwrap().to_string())
        .collect()
}

struct CppFile {
    name: String,
    time: SystemTime,
    deps: HashSet<String>,
}

impl CppFile {
    fn new(path: &Path) -> io::Result<CppFile> {
        let file = File::open(&path)?;
        let reader = BufReader::new(&file);
        let mut deps = HashSet::new();
        for line in reader.lines() {
            let line = line?;
            if line.starts_with("#include") {
                deps.insert(String::from(
                    &line[line.find('<').unwrap() + 1..line.find('>').unwrap()],
                ));
            }
        }
        Ok(CppFile {
            name: path.to_str().unwrap().to_string(),
            time: file.metadata()?.modified()?,
            deps,
        })
    }
}

fn partition3(paths: Vec<PathBuf>) -> (Vec<PathBuf>, Vec<PathBuf>, Vec<PathBuf>) {
    let mut hs = Vec::new();
    let mut cpps = Vec::new();
    let mut os = Vec::new();
    for i in paths {
        if let Some(j) = i.extension() {
            match j.to_str().unwrap() {
                "h" => hs.push(i.clone()),
                "cpp" => cpps.push(i.clone()),
                "o" => os.push(i.clone()),
                _ => {}
            }
        }
    }
    (hs, cpps, os)
}

fn get_deps(paths: &[PathBuf]) -> Vec<CppFile> {
    paths.par_iter().map(|p| CppFile::new(p).unwrap()).collect()
}

fn is_stale<'a, 'b: 'a, 'c: 'b>(
    file: &'b CppFile,
    includes: &'c HashMap<String, CppFile>,
    o_time: &SystemTime,
    seen: &mut HashSet<&'a str>,
) -> bool {
    let name = file.name.as_str();
    if seen.contains(name) {
        return false;
    }
    seen.insert(name);
    if *o_time < file.time {
        return true;
    }
    file.deps
        .iter()
        .filter(|i| includes.contains_key(i.as_str()))
        .any(|i| is_stale(includes.get(i).unwrap(), includes, o_time, seen))
}

fn all_files(dirs: &[String], extensions: &[&str]) -> Vec<PathBuf> {
    dirs.iter()
        .flat_map(|p| Path::new(p).read_dir().unwrap())
        .map(|p| p.unwrap().path())
        .filter(|f| {
            f.is_file()
                && match f.extension() {
                    None => false,
                    Some(ext) => {
                        let ext = ext.to_str().unwrap();
                        extensions.iter().any(|e| *e == ext)
                    }
                }
        })
        .collect()
}

fn stale_cpps(paths: &[String]) -> Vec<CppFile> {
    let (includes, srcs, objects) = partition3(all_files(paths, &["cpp", "h", "o"]));
    let mut object_map = HashMap::with_capacity(objects.len());
    for i in objects {
        let name = i.to_str().unwrap().to_string();
        let time = i.metadata().unwrap().modified().unwrap();
        object_map.insert(name, time);
    }

    let mut includes_map = HashMap::with_capacity(includes.len());
    let includes = get_deps(&includes);
    let num_includes = includes.len();
    for i in includes {
        includes_map.insert(
            i.name[i.name.as_str().rfind('/').unwrap() + 1..].to_string(),
            i,
        );
    }

    let mut srcs = get_deps(&srcs);
    srcs.retain(|src| {
        let obj_name = src.name.replace(".cpp", ".o");
        match object_map.get(&obj_name) {
            Some(o_time) => is_stale(
                src,
                &includes_map,
                o_time,
                &mut HashSet::with_capacity(num_includes),
            ),
            None => true,
        }
    });
    srcs
}

fn compile(dirs: &[String], symbols: &[String], is_full: bool, is_release: bool, is_test: bool) {
    let includes = dirs
        .iter()
        .map(|dir| format!("-I{}", dir))
        .collect::<Vec<_>>();
    let symbols = symbols
        .iter()
        .map(|symbol| format!("-D{}", symbol))
        .collect::<Vec<_>>();

    let mut cpp_files = stale_cpps(dirs);
    if cpp_files.is_empty() {
        return;
    }

    if is_test {
        cpp_files.retain(|f| !f.name.ends_with(".m.cpp"));
    } else {
        cpp_files.retain(|f| !f.name.ends_with(".t.cpp"));
    }

    let options = if is_full {
        vec![if is_release { "-O3" } else { "-O0" }, "-g", "-c", "-o"]
    } else {
        vec!["-fsyntax-only"]
    };
    let children = cpp_files
        .into_iter()
        .map(|f| {
            let mut command = Command::new("clang++");
            command
                .stderr(Stdio::piped())
                .arg("-std=c++14")
                .arg("-fdiagnostics-color")
                .arg("-fasynchronous-unwind-tables")
                .arg("-ffunction-sections")
                .arg("-Wall")
                .arg("-Werror")
                .args(symbols.as_slice())
                .args(includes.as_slice())
                .args(options.as_slice());
            if is_full {
                command.arg(f.name.replace(".cpp", ".o")).arg(&f.name);
            } else {
                command.arg(&f.name);
            }
            (f.name, command.spawn().unwrap())
        })
        .collect::<Vec<_>>();
    let total = children.len() + 1;

    if children
        .into_iter()
        .enumerate()
        .map(|(num, (name, mut child))| {
            progress(&format!("[{1}/{2}] Compiling {0}", name, num + 1, total));
            display_errors(&mut child);
            !child.wait().unwrap().success()
        })
        .any(|c| c)
    {
        exit(1);
    }
}

fn linker_deps(direct_deps: &[String]) -> Vec<String> {
    let mut deps = HashSet::new();
    let mut queue = direct_deps
        .iter()
        .map(|s| s.to_string())
        .collect::<VecDeque<String>>();
    while !queue.is_empty() {
        let pc = queue.pop_front().unwrap();
        let libs = String::from_utf8(
            Command::new("pkg-config")
                .arg("--libs-only-l")
                .arg(&pc)
                .output()
                .unwrap()
                .stdout,
        )
        .unwrap()
        .split_whitespace()
        .map(|s| s.to_string())
        .collect::<Vec<_>>();
        deps.extend(libs);
        let mut pkgs = String::from_utf8(
            Command::new("pkg-config")
                .arg("--print-requires-private")
                .arg(&pc)
                .output()
                .unwrap()
                .stdout,
        )
        .unwrap()
        .split_whitespace()
        .map(|s| s.to_string())
        .filter(|s| !deps.contains(s))
        .collect::<VecDeque<_>>();
        queue.append(&mut pkgs);
    }
    deps.into_iter().collect()
}

fn linking(target: &str, dirs: &[String], pkgs: &[String], is_release: bool, is_test: bool) {
    let o_files = all_files(dirs, &["o"]);
    // TODO: Add check to see if relinking is needed
    progress(&format!("[{1}/{1}] Linking {0}", target, o_files.len() + 1));
    let options = if is_release {
        vec!["-Os", "-flto", "-s"]
    } else {
        vec!["-O0"]
    };

    let test_options = if is_test {
        vec!["-L/usr/lib/", "-lgtest_main", "-lgtest"]
    } else {
        vec![]
    };

    let mut child = Command::new("clang++")
        .stderr(Stdio::piped())
        .arg("-std=c++14")
        .arg("-Wl,--gc-sections")
        .arg("-fuse-ld=lld")
        .arg("-pthread")
        .args(options.as_slice())
        .args(test_options.as_slice())
        .arg("-o")
        .arg(target.to_string() + ".exe")
        .args(o_files.as_slice())
        .args(linker_deps(pkgs).as_slice())
        .spawn()
        .unwrap();
    display_errors(&mut child);
    if !child.wait().unwrap().success() {
        exit(1);
    }
}

fn archiving(target: &str, dirs: &[String], is_test: bool) {
    // TODO
    let _ = &target;
    let _ = &dirs;
    let _ = is_test;
}

fn build(config: Config, dirs: &[String], is_release: bool) {
    let target = config.target;
    let symbols = config.symbols;
    let pkgs = config.pkgs;

    compile(dirs, &symbols, true, is_release, false);
    // TODO: This will need to be automatically determined.
    let is_exe = true;
    if is_exe {
        linking(&target, dirs, &pkgs, is_release, false);
    } else {
        archiving(&target, dirs, false);
    }
    success("The program has been compiled and linked.");
}

fn test(config: Config, dirs: &[String], is_release: bool) {
    let target = config.target;
    let symbols = config.symbols;
    let pkgs = config.pkgs;

    compile(dirs, &symbols, true, is_release, true);
    linking(&target, dirs, &pkgs, is_release, true);
    success("The tests have been compiled and linked.");
}

fn check(config: Config, dirs: &[String]) {
    let symbols = config.symbols;

    compile(dirs, &symbols, false, false, false);
    success("The program has been compiled.");
}

fn clean(config: Config, dirs: &[String]) {
    let target = config.target;

    let o_files = all_files(dirs, &["o"]);
    let _ = Command::new("rm")
        .arg("-f")
        .arg(target + ".exe")
        .args(o_files.as_slice())
        .output();
    success("Removed all temporary files");
}

fn run(config: Config, dirs: &[String], is_release: bool, args: &[&str]) {
    let target = config.target;
    let pkgs = config.pkgs;
    let symbols = config.symbols;

    // TODO: This will need to be automatically determined.
    let is_exe = true;
    if !is_exe {
        error("This is a library there is nothing to run.");
        exit(1);
    }

    compile(dirs, &symbols, true, is_release, false);
    linking(&target, dirs, &pkgs, is_release, false);

    let mut child = Command::new(format!("./{}.exe", target))
        .stderr(Stdio::piped())
        .args(args)
        .spawn()
        .unwrap();
    exit(child.wait().unwrap().code().unwrap());
}

fn new(target: &str, pkgs: Vec<&str>, symbols: Vec<&str>) -> io::Result<()> {
    let yaml_string = |k: &str, v: &str| (Yaml::String(k.to_string()), Yaml::String(v.to_string()));
    let target = yaml_string("target", target);
    let yaml_array = |k: &str, v: Vec<&str>| {
        (
            Yaml::String(k.to_string()),
            Yaml::Array(v.into_iter().map(|i| Yaml::String(i.to_string())).collect()),
        )
    };
    let pkgs = yaml_array("pkgs", pkgs);
    let symbols = yaml_array("symbols", symbols);
    let config = Yaml::Hash(vec![target, pkgs, symbols].into_iter().collect());

    let mut payload = String::new();
    {
        let mut emitter = YamlEmitter::new(&mut payload);
        emitter.dump(&config).unwrap();
    }
    let mut file = File::create(&CONFIG_NAME)?;
    file.write_all(payload.as_bytes())?;

    success(&format!("Created {}", CONFIG_NAME));
    Ok(())
}

// TODO
fn fmt(config: Config, dirs: &[String]) {
    let _ = config;
    let _ = &dirs;
}

// TODO
fn lint(config: Config, dirs: &[String]) {
    let _ = config;
    let _ = &dirs;
}

fn completions(sh: Shell) {
    build_cli().gen_completions_to("asd", sh, &mut io::stdout());
}

fn vec_from_values<'a>(list: &'a ArgMatches, name: &str) -> Vec<&'a str> {
    match list.values_of(name) {
        Some(items) => items.collect(),
        None => vec![],
    }
}

fn main() {
    let app = build_cli().get_matches();
    let subcommand = app.subcommand();
    match subcommand {
        ("new", Some(sub_m)) => {
            let name = sub_m.value_of("name").unwrap();
            let pkgs = vec_from_values(sub_m, "pkgs");
            let symbols = vec_from_values(sub_m, "symbols");
            let _ = new(name, pkgs, symbols);
        }
        ("completions", Some(sub_m)) => match sub_m.value_of("shell") {
            Some("bash") => completions(Shell::Bash),
            Some("fish") => completions(Shell::Fish),
            Some("zsh") => completions(Shell::Zsh),
            _ => unreachable!(),
        },
        (_, Some(_)) => {
            let config = get_config().unwrap();
            let dirs = get_all_dirs();
            match subcommand {
                ("build", Some(sub_m)) => {
                    let release = sub_m.is_present("release");
                    build(config, &dirs, release);
                }
                ("test", Some(sub_m)) => {
                    let release = sub_m.is_present("release");
                    test(config, &dirs, release);
                }
                ("check", _) => check(config, &dirs),
                ("clean", _) => clean(config, &dirs),
                ("run", Some(sub_m)) => {
                    let release = sub_m.is_present("release");
                    let args = vec_from_values(sub_m, "args");
                    run(config, &dirs, release, args.as_slice());
                }
                ("fmt", _) => fmt(config, &dirs),
                ("lint", _) => lint(config, &dirs),
                _ => unreachable!(),
            }
        }
        _ => unreachable!(),
    }
}
