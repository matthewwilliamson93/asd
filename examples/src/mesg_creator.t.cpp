#include <mesg_creator.h>

#include <gtest/gtest.h>
#include <string>

TEST(MesgCreator, BaseTest)
{
    EXPECT_EQ("Show me what you got!\n", createMsg());
}
