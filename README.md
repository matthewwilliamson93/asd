# asd

A simple C++ build tool designed to work with clang, lld, and pkg-config.
It is designed off some of the ideas from cargo and is meant to be very simple
to pick up and run with incredibly little overhead (unlike make).

```
$ asd
asd 0.1
Matthew Williamson <matthewwilliamon93@gmail.com>
A simple C++ build tool

USAGE:
    asd <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    build          Builds a exe
    check          Semantically checks the project
    clean          Cleans out all the temp files in the project
    completions    Generates autocompletions for the shell
    fmt            Formats given files
    help           Prints this message or the help of the given subcommand(s)
    lint           Lints given files
    new            Creates a new config file for the project
    run            Builds and runs the exe
    test           Builds a test exe
```

## TODO

- Fix filtering for linking to avoid .t.o's and .m.o's, in building and testing, respectively
- Add incremental compilation for linking too
- Optimize link line generation by short circuiting pkg-config commands needed
- Change the names for builds so release, test, and normal don't make conflicting .o's
- Add support for making archives
- Implement fmt and lint
